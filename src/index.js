const partial = (id, action) => (state, {dispatch, event}) => ({[id]: Object.assign({}, state[id], action(state[id], {dispatch: action => dispatch(partial(id, action)), event}))});
export const component = view => id => (state, bind) => view(state[id], (action, global) => global ? bind(action, global) : bind(partial(id, action)));
export default component;
