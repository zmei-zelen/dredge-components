/*global describe, it, jasmine, expect*/

import {component} from '../src/index';

describe('dredge-components', () => {
    describe('component()', () => {
        const state = {partial: {x: 1}};
        let bind, view, factory, instance, action, dispatch;

        beforeEach(() => {
            bind = jasmine.createSpy('bind');
            view = jasmine.createSpy('view').and.returnValue('result');
            action = jasmine.createSpy('action').and.returnValue({y: 2})
            dispatch = jasmine.createSpy('dispatch');
            factory = component(view);
            instance = factory('partial');
        });

        // returns a view wrapper factory, that ...
        it('returns a factory for component view instances', () => {
            expect(instance(state, bind)).toEqual('result');
        });

        it('provides a partial state to the component\'s view function given it\'s name', () => {
            instance(state, bind);
            expect(view).toHaveBeenCalledWith({x: 1}, jasmine.any(Function));
        });

        it('provides a bind function that runs actions dispatched by the view in the same partial state', () => {
            instance(state, bind);
            const componentBind = view.calls.argsFor(0)[1];

            componentBind(action);
            const isolatedAction = bind.calls.argsFor(0)[0];

            const result = isolatedAction(state, {dispatch});
            expect(action).toHaveBeenCalledWith({x: 1}, {dispatch: jasmine.any(Function), event: undefined});
            expect(result).toEqual({partial: {x: 1, y: 2}});
        });

        it('passes the event to the action', () => {
            instance(state, bind);
            const componentBind = view.calls.argsFor(0)[1];

            componentBind(action);
            const isolatedAction = bind.calls.argsFor(0)[0];

            const event = {some: 'event'};
            const result = isolatedAction(state, {dispatch, event});
            expect(action).toHaveBeenCalledWith(jasmine.any(Object), {dispatch: jasmine.any(Function), event});
        })

        it('provides a bind function that can run the actions dispatched by the view against the parent state', () => {
            instance(state, bind);
            const componentBind = view.calls.argsFor(0)[1];

            componentBind(action, true);
            expect(bind).toHaveBeenCalledWith(action, true);
        });

        it('provides a modified dispatch function to the actions that will run actions in the same partial state', () => {
            instance(state, bind);
            const componentBind = view.calls.argsFor(0)[1];

            componentBind(action);
            const isolatedAction = bind.calls.argsFor(0)[0];

            isolatedAction(state, {dispatch});
            const isolatedDispatch = action.calls.argsFor(0)[1].dispatch;

            const secondaryAction = jasmine.createSpy('secondaryAction').and.returnValue({z: 3})
            isolatedDispatch(secondaryAction);
            const isolatedSecondaryAction = dispatch.calls.argsFor(0)[0];

            const result = isolatedSecondaryAction(state, {dispatch});
            expect(action).toHaveBeenCalledWith({x: 1}, {dispatch: isolatedDispatch, event: undefined});
            expect(result).toEqual({partial: {x: 1, z: 3}});
        });
    });
});
