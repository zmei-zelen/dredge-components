const gulp = require('gulp');
const jasmine = require('gulp-jasmine');

require('babel-core/register')({
    presets: ['env']
});

gulp.task('test', () =>
    gulp
        .src('test/**/*.spec.js')
        .pipe(jasmine())
);
